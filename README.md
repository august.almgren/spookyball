# About

Spookyball is a Halloween theme single player game made in Unity which combines game mechanics from Tetris and Breakout.

THe game at first appears to play like breakout: a ball moves diagonally upwards towards a set of bricks, eventually bouncing backdown when it hits a horizontal surface, and a paddle at the bottom must be moved left and right with the `A` and `D` keys to position it below the ball in order to make it bounce back up again.

Unlike breakout, however, hitting a surface adds a new tile where the hit occurred rather than removing the tile that was hit. Similarily to Tetris, the aim of the game is to complete and entire row in order to clear it and collect points, and the game sppeds up as more and more rows are cleared, increasing the difficulty over time.

This game was created as a prototype in a group over the course of a week. The aim was to take a pre-existing game and modify some mechanic in it in order to create a new game.